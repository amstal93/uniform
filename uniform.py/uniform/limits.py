from typing import Optional


def minimum(value: float, limit: float) -> Optional[str]:
    return None if value >= limit else f"Less than {limit}"


def maximum(value: float, limit: float) -> Optional[str]:
    return None if value <= limit else f"More than {limit}"


def min_length(value: str, limit: int) -> Optional[str]:
    return None if len(value) >= limit else f"Less than {limit} symbols"


def max_length(value: str, limit: int) -> Optional[str]:
    return None if len(value) <= limit else f"More than {limit} symbols"


LIMITS = {
    "min": minimum,
    "max": maximum,
    "min_length": min_length,
    "max_length": max_length,
}
