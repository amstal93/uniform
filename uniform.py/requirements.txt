# Dependencies
uvicorn
starlette
httpx # For HTTP client
aiosmtplib # For SMTP client
pyinotify # For watching folder with configuration files

# Linting
mypy
autoflake
black
isort
